const express = require('express');
const _ = require('lodash');

const app = express();
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.get('/sum', function (req, res) {
  let sum = 0;
  if (req.query !== {} && (req.query.a || req.query.b)) {
    const a = _.isFinite(+req.query.a) ? +req.query.a : 0;
    const b = _.isFinite(+req.query.b) ? +req.query.b : 0;
    sum = a + b;
  }
  res.send(""+sum);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
