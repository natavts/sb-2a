const express = require('express');
const _ = require('lodash');

const app = express();
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.get('/name', function (req, res) {
  console.log(req.query);
  let message = "Invalid fullname";
  if (req.query !== {} && req.query.fullname) {
    const fullName = req.query.fullname;
    const result = fullName.match( /([\S]+)+/g );
    if (result && result.length <= 3) {
      const secondName = result.pop();
      let cutName = secondName;
      result.forEach((r)=> {
        console.log(r);
        cutName += ` ${r[0].toUpperCase()}.`
        //`${result.pop()} ${result[0][0]} ${result[1][0]}`
      })
      message = cutName;
      console.log(cutName);
    }
    console.log(result);
  }
  res.send(message);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
