"use strict";

const express = require('express');
const _ = require('lodash');
const fetch = require('isomorphic-fetch');
import getPCAttr, { getVolumes } from './lib/pc';


const app = express();
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.get('/', function (req, res) {
  res.send('Hello World!');
});


app.get('/pc/*', (req, res) => {
  console.log(req);
  const path = req.path.slice(4);
  getPCAttr(path, res);
});

app.get('/name', function (req, res) {
  console.log(req.query);
  let message = "no";
  if (req.query !== {} && req.query.username) {
    message = getUsername(req.query.username);
  }
  res.send(message);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

var urls = [
  "https://vk.com/skillbranch",
  "//vk.com/skillbranch",
  "skillbranch"
];

function getUsername(url) {
  url = url.split("/");
  let last = url[url.length - 1];
  if (last.match( /@/i )) {
    last = last.split("@").pop();
  }
  return "@" + last;
}

// urls.forEach(function (url) {
//   console.log(getUsername(url));
// });
